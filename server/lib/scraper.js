import axios from 'axios';
import cheerio from 'cheerio';
import fs from 'fs';

export async function getHTML(url) {
  const { data: html } = await axios.get(url);
  return html;
}

export async function getContent(html) {
  let data = [];
  const $ = cheerio.load(html);

  $(".search-rezult").each((i, elem) => {
    const item_image = $(elem).find("img").attr("src");
    data.push({
      image: item_image,
      title: $(elem).find(".text .title h5").eq(0).text().trim().replace(/\n/g, '').split("").join(""),
      price: $(elem).find(".hidden-xs span[class=price]").text().trim().replace(/€/g, '').split(" ").join("")
    });
  });

  // const artFinal = JSON.stringify(data);
  // const filename = "autobilis.json";
  // fs.writeFileSync(filename, artFinal);

  return data;
}

export async function getScraperContent() {
    const html = await getHTML('https://www.autobilis.lt/skelbimai/naudoti-automobiliai?category_id=1&page=52');
    const result = await getContent(html);

    return result;
}


