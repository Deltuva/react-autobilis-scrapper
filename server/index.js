import express from 'express';
import cors from 'cors';
import { getScraperContent } from './lib/scraper';

const app = express();
app.use(cors());

app.get(`/autobilis`, async (req, res, next) => {
  console.log(`Scraping!!`);
  const [cars] = await Promise.all([
    getScraperContent()
  ]);
  res.json({cars});
});

app.listen(2093, () => {
  console.log(`Example App running on port http://localhost:2093`);
});
