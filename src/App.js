import React from 'react';
import './styles/App.scss';

import CarListing from './components/CarList/CarListing';

import Header from './components/Header';
import Footer from './components/Footer';

const App = () => {
  return (
    <>
      <Header />
      <CarListing />
      <Footer />
    </>
  );
};

export default App;
