import React from 'react';
import { Container, Navbar, Nav, Badge } from 'react-bootstrap';
import logo from '../logo.png';
import logoHero from '../logo-home.png';

const Header = () => {
  return (
    <header className="main-header">
      <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
        <Container>
          <Navbar.Brand href="#home">
            <img
              src={logo}
              className="d-inline-block align-top"
              alt="Autobilis Logo"
            />
          </Navbar.Brand>
          {/* <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto"></Nav>
          </Navbar.Collapse> */}
        </Container>
      </Navbar>
      <div className="d-flex flex-column justify-content-center align-items-center main-cars-bg py-4">
        <img
          width="400"
          height="400"
          src={logoHero}
          className="d-inline-block align-top img-fluid"
          alt="Autobilis Logo"
        />
        <h6 className="main-cars-bg__title mb-5">
          Naudotų automobilių skelbimai.
      </h6>
      </div>
    </header>
  );
};

export default Header;