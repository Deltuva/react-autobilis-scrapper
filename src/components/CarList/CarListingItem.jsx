import React from 'react';
import searchIcon from '../../search-icon.png';
import { Col } from 'react-bootstrap';

const CarListingItem = (props) => {
  const formatTitle = (str) => {
    return str.split("/")[0];
  }

  const formatPrice = (int) => {
    return int;
  }

  const formatYears = (str) => {
    return str.split("/")[1];
  }

  const formatFullSpecification = (str) => {
    return str.split("/")[3] + '/' + str.split("/")[2] + '/' + str.split("/")[1];
  }
  return (
    <Col className="car-block" lg={3} md={4} sm={6} xs={12}>
      <a href="#" target="_blank">
        <div className="cars-suggestions cars--suggestions">
          <div className="cars-suggestions__image">
            <img src={props.image} alt="Kraunasi.." />
            <img src={searchIcon} alt="" className="search-icon" />
          </div>
          {
            props.showDetail &&
            <div className="cars-suggestions__detail">
              <span className="cars-suggestions__detail-price">
                {formatPrice(props.price)} &euro;
                </span>
              <span className="cars-suggestions__detail-manufacture">
                {formatTitle(props.title)}
              </span>
              <span className="cars-suggestions__detail-model">
                {formatYears(props.title)}
              </span>
              <span className="cars-suggestions__detail-specification">
                {formatFullSpecification(props.title)} m.
                </span>
            </div>
          }
        </div>
      </a>
    </Col>
  );
};

export default CarListingItem;