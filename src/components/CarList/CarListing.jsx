import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import Loading from '../Loading';
import CarListingItem from './CarListingItem';

const CarListing = () => {
  const [data, setData] = useState([]);
  const [url, setUrl] = useState('http://localhost:2093/autobilis');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      const result = await axios(url);

      setData(result.data.cars);
      setIsLoading(false);
    };

    fetchData();
  }, []);

  return (
    <section className="cars-section my-4">
      <Container>
        <Row>
          <Col className="no-gutter" lg={9} md={9}>
            <div className="cars">
              <Row className="no-gutters">
                {isLoading ? (
                  <Loading />
                ) : (
                    data.map((suggestion, idx) =>
                      <CarListingItem
                        key={idx}
                        image={suggestion.image}
                        title={suggestion.title}
                        price={suggestion.price}
                        showDetail={true}
                      />
                    )
                  )}
              </Row>
            </div>
          </Col>
          <Col className="no-gutter" lg={3} md={3}>
            <div className="adverts">
              <div className="adverts-block">
                <span className="adverts-block__brand">Jūsų reklama čia!</span>
                <div className="adverts-block__text">
                  300x100
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default CarListing;