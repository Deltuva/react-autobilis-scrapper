import React from 'react';
import { Container, Col } from 'react-bootstrap';

const Footer = () => {
  return (
    <footer className="main-footer" bg="dark">
      <Container>
        <Col lg={12} md={12}>
          <div className="main-footer__copyrigt">
            Developed by: Deltuva
          </div>
        </Col>
      </Container>
    </footer>
  );
};

export default Footer;