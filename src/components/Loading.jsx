import React from 'react';

const Loading = () => {
  return (
    <div className="loading">
      Laukiama atsakymo iš serverio ...
    </div>
  );
};

export default Loading;